#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <omp.h>

#define  MAX_WORKERS 4
#define  NUMBER_OF_WORDS 20000
#define  MAX_WORD_LENGTH 22 // with/without escapechar?

char words[NUMBER_OF_WORDS][MAX_WORD_LENGTH];
char revwords[NUMBER_OF_WORDS][MAX_WORD_LENGTH];
char rev[MAX_WORD_LENGTH];
const char* palindromwords[NUMBER_OF_WORDS];

int total = 0;

void import() {
  int i, j;

  FILE *f = fopen("words.txt", "r");
  for (i = 0; i < NUMBER_OF_WORDS; i++) {
    // Import all words to array
    fscanf(f, "%s", &words[i][0]);
    for(j = 0; j < strlen(words[i]); j++)
      // Convert all to lowercase
      words[i][j] = tolower(words[i][j]);
  }
  fclose(f);
}

void result(char * str) {
  int p, numb = 0;
  FILE *f = fopen("result.txt", "w");
  const char *text = str;
  if (f == NULL) {
    printf("Error opening file!\n");
    exit(1);
  }
  for (p = 0; p < total; p++) {
    fprintf(f, "%s\n", palindromwords[p]);
    numb++;
  }
  fprintf(f, "%s\n", text);
  fprintf(f, "%d\n", numb);
  fclose(f);
}

int main(int argc, char const *argv[]) {
  int outerloop, numthreads;
  double starttime, endtime, extime;

  // Convert arg to int
  numthreads = strtol(argv[1], NULL, 0);

  // Set number of threads
  omp_set_num_threads(numthreads);

  // Import words from file
  import();
  starttime = omp_get_wtime();
  // Loop trough words

  #pragma omp parallel for shared(total, palindromwords)
  for (outerloop = 0; outerloop < NUMBER_OF_WORDS; outerloop++) {
    // Reverse word
    char rev[MAX_WORD_LENGTH];
    int i, j = 0;
    memset(rev, 0, MAX_WORD_LENGTH);

    for(i = strlen(words[outerloop]); i >= 0; i--) {
      if ((*(words[outerloop]+i)) != '\0') {
        rev[j] = *(words[outerloop]+i);
        j++;
      }
    }

    if (strcmp(words[outerloop], rev) == 0) {
      #pragma omp critical
      {
        palindromwords[total] = &words[outerloop][0];
        total++;
      }
    }
    else {
      int innerloop;
      for (innerloop = outerloop; innerloop < NUMBER_OF_WORDS; innerloop++) {
        if (strcmp(words[innerloop], rev) == 0 && innerloop != outerloop) {
          #pragma omp critical
          {
            palindromwords[total] = &words[outerloop][0];
            palindromwords[total+1] = &words[innerloop][0];
            // Add 2 because the two different words are both palindromic.
            total += 2;
          }
        }
      }
    }
  }

  endtime = omp_get_wtime();
  extime = endtime - starttime;

  char str[800];
  sprintf(str, "Total: %d, Execution time: %f with %d threads.\n", total, extime, numthreads);
  result(str);
  printf("%s", str);
  return 0;
}
