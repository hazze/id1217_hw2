#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <omp.h>

#define MAX_WORKERS 2
#define  NUMBER_OF_WORDS 25143
#define  MAX_WORD_LENGTH 22 // with/without escapechar?

char words[NUMBER_OF_WORDS][MAX_WORD_LENGTH];
char rev[MAX_WORD_LENGTH];

void import() {
  int i, j;

  FILE *f = fopen("words.txt", "r");
  for (i = 0; i < NUMBER_OF_WORDS; i++) {
    // Import all words to array
    fscanf(f, "%s", &words[i][0]);
    for(j = 0; j < strlen(words[i]); j++)
      // Convert all to lowercase
      words[i][j] = tolower(words[i][j]);
  }
  fclose(f);
}

void * reverse(char * str )
{
  int i, j = 0;

  // Clear rev from old word.
  memset(rev, 0, MAX_WORD_LENGTH);

  // Reverse word and store in rev
  for(i = strlen(str); i >= 0; i--) {
    if ((*(str+i)) != '\0') {
      rev[j] = *(str+i);
      j++;
    }
  }
}

int main(int argc, char const *argv[]) {
  int k, m, total = 0;
  omp_set_num_threads(MAX_WORKERS);

  // Import words from file
  import();

  // Loop trough words
  for (k = 0; k < NUMBER_OF_WORDS; k++) {
    reverse(words[k]);
    // Check if word is true palindrom
    if (strcmp(words[k], rev) == 0) {
      total++;
    }
    else {
      for (m = 0; m < NUMBER_OF_WORDS; m++) {
        // Check if reversed word exist in list
        if (strcmp(words[m], rev) == 0) {
          total++;
        }
      }
    }
  }
    printf("%d\n", total);
    return 0;
}
