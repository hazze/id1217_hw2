#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <omp.h>

#define  MAX_WORKERS 4
#define  NUMBER_OF_WORDS 25143
#define  MAX_WORD_LENGTH 22 // with/without escapechar?

char words[NUMBER_OF_WORDS][MAX_WORD_LENGTH];
char revwords[NUMBER_OF_WORDS][MAX_WORD_LENGTH];
char rev[MAX_WORD_LENGTH];
char pal[NUMBER_OF_WORDS][MAX_WORD_LENGTH];

int b = 0;

void import() {
  int i, j;

  FILE *f = fopen("words.txt", "r");
  for (i = 0; i < NUMBER_OF_WORDS; i++) {
    // Import all words to array
    fscanf(f, "%s", &words[i][0]);
    for(j = 0; j < strlen(words[i]); j++)
      // Convert all to lowercase
      words[i][j] = tolower(words[i][j]);
  }
  fclose(f);
}

void result(char * str) {
  int p, numb = 0;
  FILE *f = fopen("result.txt", "a");
  const char *text = str;
  if (f == NULL) {
    printf("Error opening file!\n");
    exit(1);
  }
  for (p = 0; p < NUMBER_OF_WORDS; p++) {
    if(pal[p][0] != '\0') {
      fprintf(f, "%s\n", pal[p]);
      numb++;
    }
  }
  fprintf(f, "%s\n", text);
  fprintf(f, "%d\n", numb);
  fclose(f);
}

const char * reverse(char * str )
{
  int i, j = 0;
  memset(rev, 0, MAX_WORD_LENGTH);

  // Reverse word and store in rev
  for(i = strlen(str); i >= 0; i--) {
    if ((*(str+i)) != '\0') {
      rev[j] = *(str+i);
      j++;
    }
  }
  return rev;
}

int main(int argc, char const *argv[]) {
  int k, numthreads, total = 0;
  double starttime, endtime, extime;

  // Convert arg to int
  numthreads = strtol(argv[1], NULL, 0);

  // Set number of threads
  omp_set_num_threads(numthreads);

  // Import words from file
  import();
  starttime = omp_get_wtime();
  // Loop trough words
  // #pragma omp parallel for shared(revwords, total, words, pal, b)
  for (k = 0; k < NUMBER_OF_WORDS; k++) {
    // #pragma omp critical
    {
      strcpy(revwords[k], reverse(words[k]));
    }
    // Check if word is true palindrom
    if (strcmp(words[k], revwords[k]) == 0) {
      total++;
      // #pragma omp critical
      // {
        // snprintf(pal[b], sizeof(pal[b]), "%s == %s", words[k], revwords[k]);
        strcpy(pal[b], words[k]);
      // }
    }
    else {
      int m;
      for (m = 0; m < NUMBER_OF_WORDS; m++) {
        // Check if reversed word exist in list
        if (strcmp(words[m], revwords[k]) == 0) {
          total++;
          // strcpy(pal[b], words[m]);
          // #pragma omp critical
          // {
            strcpy(pal[b], words[m]);
          // }
        }
      }
    }
    b++;
  }

  endtime = omp_get_wtime();
  extime = endtime - starttime;

  char str[800];
  sprintf(str, "Total: %d, Execution time: %f with %d threads.\n", total, extime, numthreads);
  result(str);
  printf("Total: %d, Execution time: %f with %d threads.\n", total, extime, numthreads);
  return 0;
}
